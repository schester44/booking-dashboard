# Conference Booking App Dashboard #

An [Angular 1.6-based](https://bitbucket.org/appsintegrations/app-framework-customer-dashboard) dashboard for managing the Conference Room Booking App.


Refer to the [Master Repo](https://bitbucket.org/schester44/booking-api) for installation and usage.
