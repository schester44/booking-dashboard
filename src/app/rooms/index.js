import { loadAppModule, ngmodule } from "../bootstrap/ngmodule"

import { roomsState, addRoomState, editRoomState } from "./rooms.states"

import { roomsScene } from "./rooms.component"

import { RoomService } from "./services/room.service";
import { list } from "./components/list";
import { item } from "./components/item";
import { roomForm } from "./components/roomForm";

import { welcomeHero } from "../main/components/welcomeHero"

const roomsModule = {
	states: [roomsState, addRoomState, editRoomState],
	components: { roomsScene, list, item, roomForm, welcomeHero },
	services: {RoomService},
	directives: {},
	filters: {},
	configBlocks: [],
	runBlocks: []
}

loadAppModule(ngmodule, roomsModule)
