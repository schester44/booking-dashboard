export const roomsState = {
	parent: "app",
	name: "rooms",
	url: "/rooms",
	resolve: {
		themes: [
			"ThemeService",
			async ThemeService => {
				const req = ThemeService.api.query()
				return await req.$promise
			}
		],
		services: [
			"ServicesService",
			async ServicesService => {
				const req = ServicesService.api.query()
				return await req.$promise
			}
		],
		rooms: [
			"RoomService",
			async RoomService => {
				const req = RoomService.api.query()
				return await req.$promise
			}
		]
	},
	views: {
		main: "roomsScene"
	}
}

export const addRoomState = {
	name: "rooms.addRoom",
	url: "/add",
	resolve: {
		devices: [
			"DevicesService",
			async DevicesService => {
				const request = DevicesService.api.query()
				const data = await request.$promise
				return data.sort((a, b) => a.id - b.id)	
			}
		]
	},
	views: {
		roomForm: "roomForm"
	}
}

export const editRoomState = {
	name: "rooms.editRoom",
	url: "/:id/edit",
	params: {
		id: undefined
	},
	resolve: {
		room: [
			"rooms",
			"$transition$",
			async (rooms, $transition$) => {
				const allRooms = await rooms.$promise
				return allRooms.find(room => room.id.toString() === $transition$.params().id.toString())
			}
		],
		themes: [
			"ThemeService",
			async ThemeService => {
				const request = ThemeService.api.query()
				return await request.$promise
			}
		],
		devices: [
			"DevicesService",
			async DevicesService => {
				const request = DevicesService.api.query()
				const data = await request.$promise
				return data.sort((a, b) => a.id - b.id)	
			}
		]
	},
	views: {
		roomForm: "roomForm"
	}
}
