export class RoomService {
      constructor($resource, AppConfig) {
            "ngInject";

            const endPoint = `${AppConfig.apiEndpoint}/api/rooms`;

            this.api = $resource(`${endPoint}/:id`, { id: "@id" }, {
                  update: {
                        url: endPoint,
                        method: "PUT",
                        headers: {
                              "Content-Type": undefined
                        }
                  },
                  save: {
                        url: endPoint,
                        method: "POST",
                        headers: {
                              "Content-Type": undefined
                        }
                  }
            });
      }
}
