import "./roomForm.scss"

class RoomFormController {
	constructor(ActionModal, toastr, RoomService, $state, $interval) {
		"ngInject"

		this.toastr = toastr
		this.ActionModal = ActionModal
		this.RoomService = RoomService.api
		this.$state = $state

		this.groups = [1, 2, 3, 4, 5, 6, 7]
	}

	$onInit() {
		this.model = typeof this.room === "undefined" ? { service_id: 3, group: "1" } : this.room;
	}

	onFormSubmit() {
		if (this.room) {
			this.updateRoom()
		} else {
			this.createRoom()
		}
	}

	createRoom() {
		const room = new this.RoomService(this.model)

		room.$save(res => {
			this.rooms.push(res)
			this.toastr.success(`Room created.`);
			this.$state.go('rooms.editRoom', { id: res.id });
		}, err => {
			this.toastr.error(err.data.error || 'Failed to create the room. Please try again.')
		})
	}

	updateRoom() {
		this.model.$update(res => {
			// update the model with the returned data (need to do this for things like cchd_record)
			this.model = res;
			this.toastr.success(`Room updated.`);
		}, err => {
			this.toastr.error(err.data.error || 'Failed to update the room. Please try again.')
		})
	}

	deleteRoom() {
		const index = this.rooms.findIndex(room => room.id.toString() === this.room.id.toString());

		const actionCallback = () => {
			this.room.$delete(() => {
				this.rooms.splice(index, 1);
				this.toastr.success('Room deleted.');
				this.ActionModal.close();
				this.$state.go('rooms');
			}, err => {
				this.toastr.error(err.data.error || 'Failed to delete the room.')
				this.ActionModal.close();
			})
		}

		this.ActionModal.open(actionCallback, {
			heading: 'Delete Room',
			message: 'This action is irreversible. Are you sure you want to delete the room?',
			button: 'Yes, Delete'
		})
	}
}

const template = `
<section class="shaded RoomForm">
	<div class="section__heading section__heading--with-action space-between">
		<h1>{{ $ctrl.room ? 'Edit Room' : 'Add Room' }}</h1>
		<button ng-if="$ctrl.room" class="clear btn--pale" ng-click="$ctrl.deleteRoom(); $event.preventDefault();">Delete</button>
	</div>

	<form name="$ctrl.form">
		<div class="form-group float-label">
			<label for="name">Room Name</label>
			<input type="text" name="name" ng-model="$ctrl.model.name" required />
		</div>

		<div class="form-group float-label">
			<label for="store">Device</label>
			<ui-select ng-model="$ctrl.model.device_id" theme="selectize">
			    <ui-select-match>
			        <span><span ng-bind="$select.selected.name"></span> (<span ng-bind="$select.selected.id"></span>)</span>
			    </ui-select-match>
			    <ui-select-choices repeat="device.id as device in ($ctrl.devices | filter: $select.search)">
			        <span><span ng-bind="device.name"></span> (<span ng-bind="device.id"></span>)</span>
			    </ui-select-choices>
			</ui-select>
		</div>

		<div class="form-group float-label" ng-if="$ctrl.themes.length > 1">
			<label for="store">Theme</label>
			<ui-select ng-model="$ctrl.model.theme_id" theme="selectize" required>
			    <ui-select-match>
			        <span ng-bind="$select.selected.name"></span>
			    </ui-select-match>
			    <ui-select-choices repeat="theme.id as theme in ($ctrl.themes | filter: $select.search)">
			        <span ng-bind="theme.name"></span>
			    </ui-select-choices>
			</ui-select>
		</div>

		<div class="form-group float-label">
			<label for="store">Service Account</label>
			<ui-select ng-model="$ctrl.model.service_id" theme="selectize" required>
			    <ui-select-match>
			        <span ng-bind="$select.selected.display_name"></span>
			    </ui-select-match>
			    <ui-select-choices repeat="service.id as service in ($ctrl.services | filter: $select.search)">
			        <span ng-bind="service.display_name"></span>
			    </ui-select-choices>
			</ui-select>
		</div>

		<div class="form-group float-label" ng-if="$ctrl.model.service_id !== 3">
			<label for="name">Room Email</label>
			<input type="email" name="email" ng-model="$ctrl.model.email" required />
		</div>

		<div class="form-group float-label">
			<label for="store">Group</label>
			<ui-select ng-model="$ctrl.model.group" theme="selectize">
			    <ui-select-match>
			        <span>Group <span ng-bind="$select.selected"></span></span>
			    </ui-select-match>
			    <ui-select-choices repeat="group in ($ctrl.groups | filter: $select.search)">
			        <span>Group <span ng-bind="group"></span></span>
			    </ui-select-choices>
			</ui-select>
		</div>

		<div class="checkbox-group">
			<label class="checkbox">
				<input type="checkbox" ng-model="$ctrl.model.hidden">
				<i></i>
			</label>
			<label for="hidden">Stealth (Invisible to other rooms)</label>
		</div>

		<div class="checkbox-group">
			<label class="checkbox">
				<input type="checkbox" ng-model="$ctrl.model.view_all">
				<i></i>
			</label>
			<label for="view_all">Master (Can see all rooms)</label>
		</div>
		
		<div class="form-buttons flex-end">
			<button type="cancel" ui-sref="rooms" class="clear">Cancel</button>
			<button
				ng-disabled="$ctrl.form.$invalid"
				type="submit"
				ng-click="$ctrl.onFormSubmit()"
				class="btn--dark semi-wide">{{ $ctrl.room ? 'Update' : 'Create' }}
			</button>
		</div>

	</form>
</section>
`

export const roomForm = {
	template,
	controller: RoomFormController,
	bindings: {
		rooms: "=",
		room: "<",
		services: "<",
		devices: "<",
		themes: "<"
	}
}