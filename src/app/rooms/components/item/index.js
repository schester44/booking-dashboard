import "./item.scss";

const template = `
<div class="Item" ui-sref="rooms.editRoom({ id: $ctrl.item.id })">
	<h2>{{ $ctrl.item.name }}</h2>
</div>
`

export const item = {
	template,
	controller: class ItemController {},
	bindings: {
		item: "="
	}
}