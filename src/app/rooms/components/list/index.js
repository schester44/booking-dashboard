import "./list.scss"

const template = `
	<div class="List">
		<item
			ng-class="{ 'active': ($ctrl.activeItem | num) === item.id }"
			ng-repeat="item in $ctrl.items"
			item="item">
		</item>
		<h4 class="placeholder" ng-if="!$ctrl.items || $ctrl.items.length === 0">No Active Rooms.</h4>
	</div>
`

export const list = {
	template,
	controller: class ListController {},
	bindings: {
		items: "=",
		activeItem: "<"
	}
}