import "./rooms.scss"

class RoomsController {
	constructor($transitions) {
		this.$transitions = $transitions
		
	}

	$onInit() {
		this.registerHooks()
		this.setActiveRoom(this.$transition$.params().id)
	}

	registerHooks() {
		this.$transitions.onFinish({ to: state => state.name.includes("rooms") }, transition => {
			this.setActiveRoom(transition.params().id)
		})
	}

	setActiveRoom(id) {
		this.activeRoom = id
	}
}

export const roomsScene = {
	controller: RoomsController,
	bindings: {
		$transition$: "<",
		rooms: "<"
	},
	template: `
<welcome-hero visible="!$ctrl.rooms || $ctrl.rooms.length === 0"></welcome-hero>
<div class="page-container Rooms">
	<section>
		<div class="section__heading section__heading--with-action">
			<h1>Rooms</h1>
			<button ui-sref="rooms.addRoom" class="btn btn--brand">Create Room</button>
		</div>
		
		<div class="section-columns">
			<list 
				class="column"
				active-item="$ctrl.activeRoom"
				items="$ctrl.rooms">
			</list>
			<div class="column" ui-view="roomForm"></div>
		</div>
	</section>
 </div>
`
}
