import "./sidebar.scss";

import { sidebarNav } from "./components/sidebarNav"

class SidebarController {
	constructor($rootScope) {
		"ngInject"
		this.$rootScope = $rootScope
	}
	
	toggleSidebar() {
		this.expanded = !this.expanded
		this.$rootScope.$broadcast("sidebar-toggle", { expanded: this.expanded })
	}
}


const template = `
<div class="sidebar">
	<div
		class="toggle-visibility"
		ng-class="{'expand': !$ctrl.expanded }"
		ng-click="$ctrl.toggleSidebar()">
	</div>

	<div class="inner" ng-class="{ 'expanded': $ctrl.expanded }">
		<div class="sidebar__logo">
			<img ng-src="img/icon.png">
		</div>
		<sidebar-nav
			expanded="$ctrl.expanded">
		</sidebar-nav>
	</div>
</div>
`

const sidebar = {
	template,
	controller: SidebarController,
	bindings: {
		expanded: "="
	}
}


export { sidebar, sidebarNav }