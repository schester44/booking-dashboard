import "./sidebarSubNav.scss"

class SidebarSubNavController {}

const template = `
<ul class="navigation">
	<li ui-sref-active="active">
		<i class="fa fa-home" aria-hidden="true"></i>
	</li>
	
	<li ui-sref-active="active">
		<i class="fa fa-map" aria-hidden="true"></i>
	</li>
	
	<li ui-sref-active="active">
		<i class="fa fa-bookmark" aria-hidden="true"></i>
	</li>
</ul>
`

export const sidebarSubNav = {
	template,
	controller: SidebarSubNavController,
}
