import "./sidebarNav.scss";

class SidebarNavController {
	constructor($scope) {
		"ngInject"

		$scope.$on('sidebar-toggle', (ev, sidebar) => {
			// do something
		})
	}
}

const template = `
<ul class="navigation">
	<li ui-sref="rooms" ui-sref-active="active">
		<i class="fa fa-map-marker"></i>
		<span>Rooms</span>
	</li>
	
	<li ui-sref="settings" ui-sref-active="active">
		<i class="fa fa-cog"></i>
		<span>Settings</span>
	</li>
	
</ul>
`

export const sidebarNav = {
	template,
	controller: SidebarNavController
}