import "./navbar.scss";

class NavbarController {}

const template = `
  <div class="app-navbar">
    
    <div class="fixed">
      
      <div class="app-navbar__inner">

        <div class="title">
            <img iw-src="img/icon.png" class="logo" ui-sref="home">
            <h1>Booking App</h1>
        </div>

      </div>

    </div>
    
  </div>
`

export const navbar = {
  template,
  controller: NavbarController,
};
