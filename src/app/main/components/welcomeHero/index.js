import "./welcomeHero.scss";


class WelcomeHeroController {
	constructor() {}

	$onInit() {
		const value = localStorage.getItem('display-welcome-hero');
		this.visible = value === null ? (typeof this.visible === "undefined" ? true : this.visible) : value;
	}

	close() {
		this.visible = false;
		localStorage.setItem('display-welcome-hero', false);
	}
}


const template = `
<div class="WelcomeHero" ng-if="$ctrl.visible === true">
	<i class="fa fa-calendar" aria-hidden="true"></i>
	<i class="fa fa-times" ng-click="$ctrl.close()"></i>

	<div class="WelcomeHero__inner">
		<h1>Welcome!</h1>
		<p class="sub">It appears this is your first time here. No worries, getting started is a breeze!</p>
		<div class="instructions">
			<p class="title">DETAILS:</p>
			<p>You'll need to <span ui-sref="settings">configure the app</span> first if you're using a calendar sevice such as iCal or Outlook/Exchange.</p>
			<p>Or you can jump right in and <span ui-sref="rooms.addRoom">create a room</span> if you'd prefer to use "offline" mode.</p>
			<p class="footer">Need further assistance? Read the <a href="#">help documentation</a>.</p>
		</div>
	</div>
</div>
`


export const welcomeHero = {
	template,
	controller: WelcomeHeroController,
	bindings: {
		visible: "<"
	}
}