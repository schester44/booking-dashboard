import "./404.scss";

class FourOhFourController {}

const template = `
<div class="fohf">
	<h1>Oops!</h1>
	<h2>Looks like this page doesn't exist.</h2>

	<button class="cta" ui-sref="home">Go Back</button>
</div>
`

export const fourOhFour = {
	template,
	controller: FourOhFourController
};
