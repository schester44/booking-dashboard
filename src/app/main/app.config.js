/**
 * Application-wide configuration blocks.
 */

export const otherwiseConfigBlock = [
	"$urlRouterProvider",
	$urlRouterProvider => {
		$urlRouterProvider.otherwise("/rooms")
	}
];
export const html5ModeConfigBlock = [
	"$locationProvider",
	$locationProvider => {
		$locationProvider.html5Mode(true)
	}
];

export const toastrConfigBlock = [
	"toastrConfig",
	toastrConfig => {
		angular.extend(toastrConfig, {
			extendTimeout: 3000,
			positionClass: "toast-top-right",
			maxOpened: 5
		})
	}
];
