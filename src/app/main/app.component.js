import "./app.scss"

class AppController {
  constructor() {
    "ngInject"
    this.sidebarExpanded = true
  }
}

const template = `
<div class="skeleton-bottom">
      <sidebar
        expanded="$ctrl.sidebarExpanded">
      </sidebar>
      
      <div
        class="app-container"
        ng-class="{ 'with-sidebar': $ctrl.sidebarExpanded, 'extended-sidebar': $ctrl.sidebarExpanded }"
        ui-view="main">
      </div>
</div>
`

export const app = {
  template,
  controller: AppController,
}
