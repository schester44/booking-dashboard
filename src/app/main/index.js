import { loadAppModule, ngmodule } from "../bootstrap/ngmodule";

import { app } from "./app.component";
import { fourOhFour } from "./components/fourOhFour";

import { navbar } from "./components/navbar";
import { sidebar, sidebarNav } from "./components/sidebar";

import { appState, fourOhFourState } from "./app.states";
import { otherwiseConfigBlock, toastrConfigBlock } from "./app.config";

import "font-awesome/css/font-awesome.css"
import "ng-dialog/css/ngDialog.min.css"

const mainAppModule = {
	components: { app, fourOhFour, navbar, sidebar, sidebarNav },
	states: [appState, fourOhFourState],
	filters: {},
	configBlocks: [otherwiseConfigBlock, toastrConfigBlock],
	runBlocks: []
};

loadAppModule(ngmodule, mainAppModule);
