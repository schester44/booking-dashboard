export const appState = {
	name: "app",
	redirectTo: "home",
	resolve: {
		// any application-wide API data
	},
	component: "app"
}

export const fourOhFourState = {
	name: "app.404",
	url: "/404",
	views: {
		main: "fourOhFour"
	}
}
