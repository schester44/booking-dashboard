import { transformRequest } from "../../global/utils/transformRequest";


export class SettingsService {
      constructor($resource, AppConfig) {
            "ngInject";

            const endPoint = `${AppConfig.apiEndpoint}/api/settings`;

            this.api = $resource(`${endPoint}/:id`, { id: "@id" }, {
                  update: {
                        url: endPoint,
                        method: "PUT",
                        headers: {
                              "Content-Type": undefined
                        }
                  }
            });
      }
}
