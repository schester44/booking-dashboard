import { transformRequest } from "../../global/utils/transformRequest";

export class ThemeService {
      constructor($resource, AppConfig) {
            "ngInject";

            const endPoint = `${AppConfig.apiEndpoint}/api/themes`;

            this.api = $resource(`${endPoint}/:id`, { id: "@id" }, {
                  update: {
                        transformRequest,
                        url: `${endPoint}/:id/update`,
                        method: "POST",
                        headers: {
                              "Content-Type": undefined
                        }
                  },
                  save: {
                        url: endPoint,
                        method: "POST",
                        headers: {
                              "Content-Type": undefined
                        }
                  }
            });
      }
}
