import "./settings.scss";

class SettingsContainer {
	constructor() {
		"ngInject";
	}
}

export const settingsScene = {
	controller: SettingsContainer,
	bindings: {},
	template: `
<div class="page-container Settings">
	<section>
		<div class="section__heading">
			<h1>Settings</h1>
		</div>

		<section class="shaded">
			<div class="setting-type" ui-sref="settings.general">
				<i class="fa fa-cog"></i>
				<div>
					<h4>General</h4>
					<span>View and update your app details</span>
				</div>
			</div>

			<div class="setting-type" ui-sref="settings.theme">
				<i class="fa fa-paint-brush"></i>
				<div>
					<h4>Theme</h4>
					<span>Customize the look and feel of your app</span>
				</div>
			</div>

			<div class="setting-type" ui-sref="settings.services">
				<i class="fa fa-calendar"></i>
				<div>
					<h4>Services</h4>
					<span>Add and configure your service accounts</span>
				</div>
			</div>
		</section>
	</section>
 </div>
`
};
