import { loadAppModule, ngmodule } from "../bootstrap/ngmodule"

import {
	settingsState,
	themeSettingsState,
	servicesSettingsState,
	generalSettingsState,
	newThemeState,
	editThemeState
} from "./settings.states"

import { ThemeService } from "./services/Theme.service"
import { SettingsService } from "./services/Settings.service"

import { settingsScene } from "./settings.component.js"
import { servicesSettings } from "./components/services"
import { generalSettings } from "./components/general"
import { themeSettings } from "./components/theme"
import { themeManager } from "./components/theme/components/themeManager"

import imagePicker from "./components/theme/components/imagePicker"
import ImagePickerService from "./components/theme/components/imagePicker/imagePicker.service"

const settingsModule = {
	states: [
		settingsState,
		themeSettingsState,
		servicesSettingsState,
		generalSettingsState,
		newThemeState,
		editThemeState
	],
	components: { settingsScene, servicesSettings, generalSettings, themeSettings, themeManager, imagePicker },
	services: { SettingsService, ThemeService, ImagePickerService },
	directives: {},
	filters: {},
	configBlocks: [],
	runBlocks: []
}

loadAppModule(ngmodule, settingsModule)
