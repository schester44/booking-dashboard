import timezones from "./timezones.js";

export const settingsState = {
	parent: "app",
	name: "settings",
	url: "/settings",
	resolve: {
		settings: [ "SettingsService", async SettingsService => {
			const request = SettingsService.api.get()
			return await request.$promise
		}],
		timezones: () => {
			return timezones
		}
	},
	views: {
		main: "settingsScene"
	}
}

export const themeSettingsState = {
	name: "settings.theme",
	url: "/theme",
	resolve: {
		themes: [ "ThemeService", async ThemeService => {
			const request = ThemeService.api.query();
			return await request.$promise;
		} ]
	},
	views: {
		"!$default.main": "themeSettings"
	}
}

export const newThemeState = {
	name: "settings.theme.new",
	url: "/new",
	views: {
		"!$default.main": "themeManager"
	}
}


export const editThemeState = {
	name: "settings.theme.edit",
	url: "/:id",
	params: {
		id: undefined
	},
	resolve: {
		theme: [
			"themes",
			"$transition$",
			async (themes, $transition$) => {
				const allThemes = await themes.$promise;
				return allThemes.find(theme => theme.id.toString() === $transition$.params().id.toString());
			}
		]
	},
	views: {
		"!$default.main": "themeManager"
	}
}

export const servicesSettingsState = {
	name: "settings.services",
	url: "/services",
	views: {
		"!$default.main": "servicesSettings"
	},
	resolve: {
		services: [
			"ServicesService",
			async ServicesService => {
				const request = ServicesService.api.query()
				return await request.$promise
			}
		]
	}
}

export const generalSettingsState = {
	name: "settings.general",
	url: "/general",
	views: {
		"!$default.main": "generalSettings"
	}
}
