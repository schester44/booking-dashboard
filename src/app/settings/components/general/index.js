class GeneralSettingsController {
	constructor(toastr) {
		"ngInject"
		this.toastr = toastr
	}

	$onInit() {
		this.defaultValues = angular.copy(this.settings)
	}

	submitForm() {
		this.settings.$update(
			res => {
				this.toastr.success(`General Settings updated.`)
			},
			err => {
				this.toastr.error(err.data.error || `Failed to update the settings.`)
			}
		)
	}

	reset() {
		this.settings = angular.copy(this.defaultValues)
	}
}

const template = `
<div class="page-container Settings Settings--General">
 <section>
 	<button class="btn btn--nav-back clear" ui-sref="settings">
 		< Settings
 	</button>
	
	<div class="section__heading">
	 	<h1>General Settings</h1>
	</div>

	<section class="shaded">
 		<div class="sub-section">			
			<div class="section-item">
				<div class="checkbox-group">
					<label class="checkbox">
						<input type="checkbox" ng-model="$ctrl.settings.queue_event_creation">
						<i></i>
					</label>
				</div>	
				<div class="details">
					<h4>Turbo Boost</h4>
					<p>This is an experimental feature that can speed up the booking process but increases the posibility of scheduling conflicts.</p>
				</div>
			</div>

			<div class="section-item">
				<div class="checkbox-group">
					<label class="checkbox">
						<input type="checkbox" ng-model="$ctrl.settings.single_cchd_asset">
						<i></i>
					</label>
				</div>	
				<div class="details">
					<h4>Single CCHD Asset</h4>
					<p>
						By default, the app will create an HTML asset inside CommandCenter for each of the created Rooms. By enabling this feature, all rooms will share a single asset. This feature is handy when there are many devices and all devices have access to CCHDAPI. Features that rely on a "Default Room", such as themes, will not work if this is enabled when there is no access to CCHDAPI.
					</p>
				</div>
			</div>

 			<div class="form-group" style="border-top:1px solid rgba(244, 246, 248, 1.0); padding-top: 15px;">
				<h4 style="margin-bottom: 7px">Timezone</h4>

				<ui-select ng-model="$ctrl.settings.timezone" theme="selectize">
			    	<ui-select-match>
			        	<span ng-bind="$select.selected"></span>
			    	</ui-select-match>
			    	<ui-select-choices repeat="timezone in ($ctrl.timezones | filter: $select.search) track by $index">
		        		<span>{{ timezone }}</span>
			    	</ui-select-choices>
				</ui-select>
 			</div>

		</div>
		<div class="form-buttons flex-end">
			<button class="clear" ng-click="$ctrl.reset()">Reset</button>
			<button class="action" ng-click="$ctrl.submitForm()">Update</button>
		</div>

	</seciton>

</section>
</div>
`

export const generalSettings = {
	template,
	controller: GeneralSettingsController,
	bindings: {
		timezones: "<",
		settings: "<"
	}
}
