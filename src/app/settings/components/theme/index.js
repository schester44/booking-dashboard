import { createFileInput } from "../../../global/utils/createFileInput"

import "./theme.scss"

class ThemeController {
	constructor(ngDialog, ThemeService, toastr, $state, $transitions) {
		"ngInject"
		this.dialog = ngDialog
		this.service = ThemeService.api
		this.toastr = toastr
		this.$state = $state
		this.$transitions = $transitions;
	}
	
	$onInit() {
		this.registerHooks()
		this.setActiveTheme(this.$transition$.params().id)		
	}

	registerHooks() {
		this.$transitions.onFinish({ to: state => state.name.includes("theme") }, transition => {
			this.setActiveTheme(transition.params().id)
		})
	}

	setActiveTheme(id) {
		this.activeTheme = id
	}

	onCreateTheme() {
		const do_action = (name) => {
			const Resource = new this.service({ name })

			Resource.$save(res => {
				this.toastr.success(`Theme created.`)
				this.themes.push(res);
				this.dialog.close();

				this.$state.go('settings.theme.edit', { id: res.id })
			}, err => {
				this.toastr.error(err.data.error || `Failed to create the theme. Try again or contact support.`)
			})
		}

		this.dialog.open({
            appendClassName: "IWModal Settings--Theme__AddThemeModal",
            width: 600,
            controllerAs: "$ctrl",
			template: require('./addTheme.template.html'),
			plain: true,
			controller: function() {
				this.action = do_action
			}
		})
	}

	$onDestroy() {
		if (this.input) {
			this.input.removeEventListener("change", this.onFileSelected)
		}
	}
}

const template = `
<div class="page-container Settings Settings--Theme">
 <section>
 	<button class="btn btn--nav-back clear" ui-sref="settings">
 		< Settings
 	</button>

 	<div class="section__heading section__heading--with-action">
		<h1>Themes</h1>
		<button ng-click="$ctrl.onCreateTheme()">Create New</button>
 	</div>

 	<div class="section-columns">
		<div class="column theme-list">
			<div
				ng-class="{ 'active': ($ctrl.activeTheme | num) === theme.id }"
				class="theme-item"
				ng-repeat="theme in $ctrl.themes"
				ui-sref="settings.theme.edit({ id: theme.id })">
				<h2>{{ theme.name }}</h2>
			</div>

 			<p class="placeholder" ng-if="!$ctrl.themes || $ctrl.themes.length === 0">0 active themes, how about <span ui-sref="settings.themes.new">creating one</span>?</p>
		</div>

		<div class="column col-60" ui-view="themeManager"></div>	
 	</div>
 
 </section>
 </div>
`

export const themeSettings = {
	template,
	controller: ThemeController,
	bindings: {
		$transition$: "<",
		themes: '<'
	}
}
