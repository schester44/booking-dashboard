import "./themeManager.scss"
import { createFileInput } from "../../../../../global/helpers/createFileInput"

class ThemeManagerController {
	constructor(ActionModal, toastr, $state, $scope, ImageService, ImagePickerService) {
		"ngInject"
		this.ActionModal = ActionModal
		this.toastr = toastr
		this.$state = $state
		this.$scope = $scope
		this.ImageService = ImageService
		this.ImagePickerService = ImagePickerService
		this.onImageSelect = this.onImageSelect.bind(this)
	}

	$onInit() {
		this.original = angular.copy(this.theme)

		this.colorPickerOptions = {
			restrictToFormat: true,
			format: "rgb"
		}

		this.setTab("details")
	}

	async setTab(tab) {
		this.activeTab = tab

		// delay the Images api request until the images tab is clicked on
		// only make the request if we haven't made it before
		if (tab === "images" && !this.images) {
			const request = this.ImageService.api.query()
			this.images = await request.$promise
			this.setImages()
		}
	}

	setImages() {
		if (this.theme && this.theme.images) {
			this.temp_images = {
				logo: this.theme.images.logo ? this.theme.images.logo.thumbnail_url : undefined,
				background: this.theme.images.background ? this.theme.images.background.thumbnail_url : undefined
			}

			this.$scope.$apply()
		}
	}

	onImageSelect(event) {
		const image_type = event.path[0].name
		this.theme[image_type] = this.input[image_type].files[0]
	}

	selectImage(type) {
		this.ImagePickerService.open({ images: this.images },
			image => {
				if (!this.theme.images) {
					this.theme.images = {}
				}

				if (!this.temp_images) {
					this.temp_images = {}
				}

				this.theme.images[type] = image
				this.temp_images[type] = image.thumbnail_url
				this.ImagePickerService.close()
			}
		)
	}

	onDeleteTheme() {
		const actionCallback = () => {
			const index = this.themes.findIndex(theme => theme.id.toString() === this.$transition$.params().id.toString())

			this.themes[index].$delete(res => {
					this.themes.splice(index, 1)
					this.toastr.success("Theme deleted")
					this.$state.go("settings.theme")
				},
				err => {
					this.toastr.error(
						err.data.error ? err.data.error : "Failed to delete theme. Please try again or contact support"
					)
				}
			)

			this.ActionModal.close()
		}

		this.ActionModal.open(actionCallback, {
			heading: "Delete Theme",
			subHeading: "Are you sure you want to delete this theme?",
			button: "Yes, Delete"
		})
	}

	update() {
		this.theme.$update(res => {
				this.toastr.success(`Theme updated`)
				this.original = angular.copy(this.theme)
			},
			err => {
				this.toastr.error(err.data.error ? error.data.error : `Failed to update the theme.`)
			}
		)
	}

	reset() {
		this.theme = angular.copy(this.original)
	}
}

const template = `

<div class="page-container Settings Settings--Theme ThemeManager">
 <section>
 	<button class="btn btn--nav-back clear" ui-sref="settings.theme">
 		< Themes
 	</button>

 	<div class="section__heading section__heading--with-action space-between">
		<h1>{{ $ctrl.theme.name }}</h1>
		<button ng-if="$ctrl.theme.id !== 1" ng-click="$ctrl.onDeleteTheme()">Delete Theme</button>
 	</div>

		<section class="shaded">
			<div class="ThemeManager__nav">
				<ul>
					<li ng-class="{ 'active': $ctrl.activeTab === 'details' }" ng-click="$ctrl.setTab('details')">Details</li>
					<li ng-class="{ 'active': $ctrl.activeTab === 'colors' }" ng-click="$ctrl.setTab('colors')">Colors</li>
					<li ng-class="{ 'active': $ctrl.activeTab === 'images' }" ng-click="$ctrl.setTab('images')">Images</li>
				</ul>
			</div>

			<form name="$ctrl.form" ng-submit="$ctrl.submitForm()">
				
				<div class="theme-section" ng-show="$ctrl.activeTab === 'colors'">
					<div class="color-item" ng-repeat="(key, color) in $ctrl.theme.properties">

                  		<color-picker
                  			options="$ctrl.colorPickerOptions"
                  			ng-model="$ctrl.theme.properties[key].value">
                  		</color-picker>

						<div class="details">
							<h4>{{ color.name }}</h4>
							<p>{{ color.description }}</p>
						</div>
					</div>
				</div>

				<div class="theme-section" ng-show="$ctrl.activeTab === 'details'">
					<div class="form-group float-label">
						<label for="name">Theme Name</label>
						<input type="text" ng-model="$ctrl.theme.name">
					</div>
				</div>
				
				<div class="section--images theme-section" ng-show="$ctrl.activeTab === 'images'">
					
					<div class="color-item">
						<div class="item-image" ng-click="$ctrl.selectImage('background')">
							<div
								class="image"
								ng-style="{ 'background-image': 'url(' + $ctrl.temp_images.background + ')' }"
								ng-if="$ctrl.temp_images.background">
							</div>
						</div>
						<div class="details">
							<h4>Background</h4>
						</div>
					</div>

					<div class="color-item">
						<div class="item-image"
							ng-click="$ctrl.selectImage('logo')">
							<div
								class="image"
								ng-style="{ 'background-image': 'url(' + $ctrl.temp_images.logo + ')' }"
								ng-if="$ctrl.temp_images.logo">
							</div>
						</div>
						<div class="details">
							<h4>Logo</h4>
						</div>
					</div>
				</div>

				<div class="form-buttons flex-end">
					<button type="cancel" ng-click="$ctrl.reset()" class="clear">Reset</button>
					<button type="submit" class="action" ng-click="$ctrl.update()">Update</button>
				</div>
			</form>
		</section>
 
 </section>
 </div>
`

export const themeManager = {
	template,
	controller: ThemeManagerController,
	bindings: {
		$transition$: "<",
		themes: "<",
		theme: "<",
		images: "<"
	}
}
