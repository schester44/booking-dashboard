class ImagePickerService {
    constructor(ngDialog) {
        "ngInject"
        this.dialog = ngDialog
    }

    open(opts, callback) {
        return this.dialog.open({
            plain: true,
            width: 800,
            appendClassName: "IWModal ImagePicker width-800",
            template: require("./imagePicker.template.html"),
            controllerAs: "$ctrl",
            controller: function() {
                for (let [k, v] of Object.entries(opts)) {
                    this[k] = v
                }

                // Initialize a model - the selected image will be assigned to this.
                // We then pass it back in the callback
                this.model = undefined

                if (typeof this.images === "undefined") {
                    console.warn(`ImagePickerService is missing required prop: images`);
                }

                this.categories = [
                    ...new Set(this.images.filter(image => image.category !== null).map(image => image.category))
                ]

                this.use = () => {
                    callback(this.model)
                }
            }
        })
    }

    close() {
        this.dialog.close()
    }
}

export default ImagePickerService
