import "./imagePicker.scss"

import { createNode, deleteNode, generateUid, lastIteration } from "./helpers"

/**
 * Angular 1.6 @IW/ImagePicker
 *
 * $timeout - https://docs.angularjs.org/api/ng/service/$timeout
 * $scope - https://docs.angularjs.org/guide/scope
 * toastr - https://github.com/Foxandxss/angular-toastr
 * ActionModal - @IW/ActionModal - ngDialog modal wrapper
 * ImageService - @IW/ImagePicker/Service ngResource API Endpoint
 */
class ImagePickerController {
	constructor($timeout, $scope, toastr, ActionModal, ImageService) {
		"ngInject"

		this.$timeout = $timeout
		this.$scope = $scope
		this.toastr = toastr
		this.ActionModal = ActionModal
		this.Images = ImageService.api

		this.hoveringOn = undefined
	}

	$onInit() {
		// create a duplicate of this.images so we always have a "default" to fall back to
		this.displayImages = angular.copy(this.images)
	}

	$postLink() {
		this.$timeout(() => {
			this.uploader = document.getElementById("file-selector")
			this.parent = document.getElementById("image-list")
			this.addImgBtn = document.getElementById("addImgBtn")
			this.uploader.addEventListener("change", this.onImageUpload.bind(this))
		}, 0)
	}
	
	/**
	 * When a user clicks on an image, this will set the current model to that image.
	 * @param  {object} image
	 * @return {void}
	 */
	onSelectImage(image) {
		this.model = image
	}

	/**
	 * Sets hoveringOn to the ID of the image being hovered on.
	 * This is for dictating style states and what not
	 * @param  {int} id - image id
	 * @return {void}
	 */
	onImageMouseover(id) {
		this.hoveringOn = id
	}

	/**
	 * Sets hoveringOn to undefined which will hide any elements that depend on it's value being truthy
	 * @param  {int} id - iamge id
	 * @return {void}
	 */
	onImageMouseleave(id) {
		if (id === this.hoveringOn) {
			this.hoveringOn = undefined
		}
	}

	/**
	 * Deletes images by ID
	 * @param  {int} id - image id
	 * @return {void}
	 */
	onDeleteImage(id) {
		// find the image by its ID
		const img = this.images.find(image => image.id.toString() === id.toString())

// TODO -- dialog.id is undefined
		// function that runs when the user clicks the confirm button
		const actionCallback = () => {
			// delete the image
			this.Images.delete(
				{ id: id },
				res => {
					// find its position in both arrays
					const index = this.images.findIndex(image => image.id.toString() === id.toString())
					const displayIndex = this.displayImages.findIndex(image => image.id.toString() === id.toString())

					// remove the image from the arrays
					this.images.splice(index, 1)

					if (displayIndex !== -1) {
						this.displayImages.splice(displayIndex, 1)
					}
					// close the modal
					this.ActionModal.close(this.dialog.id);
					// show success notification
					this.toastr.success("Image deleted")
				},
				err => {
					// on failur, close the modal and show an error
					this.ActionModal.close(this.dialog.id);
					this.toastr.error(err.data.error || "Image was not deleted")
				}
			)
		}

		this.dialog = this.ActionModal.open(actionCallback, this.ActionModal.language.deleteImage)
	}

	/**
	 * Callback for the onChange event for the file input
	 * @param  {object} event - the onChange event
	 */
	onImageUpload(event) {
		const total_files = event.target.files.length
		let uploaded_images_count = 0

		// gets called on the for loops last iteration
		const finished = () => {
			this.toastr.success(`${uploaded_images_count} image${uploaded_images_count > 1 ? "s" : ""} uploaded.`)
		}

		for (let i = 0; i < total_files; i++) {
			const id = generateUid()

			// create and insert a temporary DOM element with the image
			this.insertTempImage(event.target.files[i], id)
			
			// save the image
			this.Images.save(
				{ name: event.target.files[i].name, file: event.target.files[i], category: this.category },
				res => {
					// delete the temp image
					deleteNode(id)
					
	                if (typeof this.images === "undefined") {
                    	this.images = []
                    	this.displayImages = []
                	}

					// add the real image
					this.images.unshift(res)
					this.displayImages.unshift(res)

					uploaded_images_count++

					// run our onFinish callback
					if (lastIteration(i, total_files)) {
						finished()
					}
				},
				err => {
					// delete the temp image
					deleteNode(id)
				
					// run our onFinish callback
					if (uploaded_images_count > 0 && lastIteration(i, total_files)) {
						finished()
					}
				
					this.toastr.error(err.data.error || "Failed to upload image")
				}
			)
		}
	}

	insertTempImage(file, id) {
		// create a new reader
		let reader = new FileReader()

		reader.onload = e => {
			// TODO --- instead of creating a DOM element and inserting it in the DOM,
			// We should insert the result directly into the array
			this.addImgBtn.parentNode.insertBefore(createNode({ id, src: e.target.result, classNames: "image uploading" }), this.addImgBtn.nextSibling)
		}

		reader.readAsDataURL(file)
	}

	$onDestroy() {
		this.uploader.removeEventListener("change", this.onImageUpload.bind(this))
	}
}

const template = `
<div class="filters">
	
	<input type="text" ng-model="$ctrl.search" placeholder="Search...">

	<div class="filters__right" ng-if="$ctrl.categories && $ctrl.categories.length > 0">
		<category-filter
			filter="$ctrl.categoryFilter"
			images="$ctrl.images"
			categories="$ctrl.categories"
			display-images="$ctrl.displayImages">
		</category-filter>
	</div>

</div>

<div id="image-list">
    <label class="image add-image-btn" id="addImgBtn">
	   	<input
	   		id="file-selector"
	   		type="file"
	   		ng-model="$ctrl.model"
	   		accept="image/*"
	   		multiple>
	</label>
	
	<div
		class="image uploaded"
		ng-mouseover="$ctrl.onImageMouseover(image.id)"
		ng-mouseleave="$ctrl.onImageMouseleave(image.id)"
		id="image-{{ ::image.id }}"
		ng-class="{ 'selected': $ctrl.model && image.id === $ctrl.model.id }"
		ng-repeat="(key, image) in $ctrl.displayImages | filter: $ctrl.search"
		ng-click="$ctrl.onSelectImage(image)">

		<i
			class="fa fa-trash"
			ng-show="$ctrl.hoveringOn == image.id"
			ng-click="$ctrl.onDeleteImage(image.id); $event.stopPropagation()">
		</i>

		<img ng-src="{{ ::image.thumbnail_url }}">
	</div>
</div>
`

const imagePicker = {
	template,
	controller: ImagePickerController,
	bindings: {
		model: "=",
		images: "<",
		categories: "<",
		category: "<"
	}
}

export default imagePicker
