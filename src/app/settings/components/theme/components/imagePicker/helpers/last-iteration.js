const lastIteration = (index, length) => index + 1 === length
export default lastIteration