const createNode = ({ src, id, classNames}) => {
	let img = document.createElement("img")
	let p = document.createElement("div")

	img.src = src

	p.setAttribute("id", id)
	p.className = classNames ? classNames : "image uploading"
	p.appendChild(img)
	
	return p
}

export default createNode