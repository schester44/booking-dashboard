const deleteNode = id => {
	const tempNode = document.getElementById(id)

	if (tempNode) {
		tempNode.parentNode.removeChild(tempNode)
	}
}

export default deleteNode