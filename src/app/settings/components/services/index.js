import "./services.scss";

import { createFileInput } from "../../../global/helpers/createFileInput";

class ServicesController {
	constructor($scope, toastr, ServicesService) {
		"ngInject";
		this.$scope = $scope
		this.toastr = toastr
		this.ServicesService = ServicesService.api

		this.onFileSelect = this.onFileSelect.bind(this)
	}

	setActiveService() {
		this.activeService = this.services.find(service => service.id.toString() === this.model.id.toString())
		
		this.model = { id: this.activeService.id };

		this.activeService.fields.forEach(field => {
			this.model[field.name] = field.value
		})
	}

	update() {
		const info = new this.ServicesService(this.model);

		info.$update(res => {
			this.toastr.success(`Updated the ${this.activeService.name} service account.`)
		}, err => {
			this.toastr.error(err.data.error || `Failed to update ${this.activeService.name} service account. Please try again or contact support.`)
		})
	}

	openFileSelect(field_name) {
		this.input = createFileInput({
			data: {
				field_name
			},
			accept: "application/json",
			events: {
				change: this.onFileSelect
			}
		})


		this.input.click();
	}

	onFileSelect(event) {
		if (typeof this.input.files[0] !== "undefined") {
			this.model[this.input.data.field_name] = this.input.files[0]
		}

		this.$scope.$apply();
	}

	$onDestroy() {
		if (this.input) {
			this.input.removeEventListener("change", this.onFileSelect)
		}
	}
}

export const servicesSettings = {
	controller: ServicesController,
	bindings: {
		services: "="
	},
	template: `
<div class="page-container Settings Settings--Services">
 <section>
 	<button class="btn btn--nav-back clear" ui-sref="settings">
 		< Settings
 	</button>

	<div class="section__heading">
 		<h1>Services Settings</h1>
	</div>

 	<section class="shaded">
 		<p class="unselected-placeholder" ng-if="!$ctrl.activeService">Select a service to configure</p>
 		<form name="$ctrl.form">
			<div class="form-group float-label">
				<label for="store">Service</label>
				<ui-select ng-model="$ctrl.model.id" theme="selectize" ng-change="$ctrl.setActiveService()">
				    <ui-select-match>
				        <span ng-bind="$select.selected.display_name"></span>
				    </ui-select-match>
				    <ui-select-choices repeat="service.id as service in ($ctrl.services | filter: $select.search)">
				        <span ng-bind="service.display_name"></span>
				    </ui-select-choices>
				</ui-select>
			</div>

			<div class="service-settings">
				<p class="placeholder" ng-if="$ctrl.activeService.fields.length === 0">
					<span>{{ $ctrl.activeService.name }}</span> mode works out of the box with no additional configuration needed.
				</p>

				<div class="form-group float-label" ng-repeat="field in $ctrl.activeService.fields">
					<div ng-switch="field.type">
	
						<div ng-switch-when="text,password,email" ng-switch-when-separator=",">
							<label for="{{ field.name }}">{{ field.display_name }}</label>
					  		<input
					  			type="{{ field.type }}"
						  			name="{{ field.name }}"
						  			ng-model="$ctrl.model[field.name]"
									ng-required="field.required"
									ng-class="{'has-error': $ctrl.form[field.name].$invalid && !$ctrl.form[field.name].$pristine}">
						</div>

					  	<div ng-switch-when="file">
					  		<button
					  			ng-click="$ctrl.openFileSelect(field.name); $event.preventDefault()">
					  				{{ !$ctrl.input.files || $ctrl.input.files.length === 0 ? 'Select ' + field.display_name : field.display_name + ' Selected' }}
					  		</button>
					  	</div>
					</div>
				</div>

			</div>

			<div class="form-buttons flex-end" ng-if="$ctrl.activeService.name !== 'offline' && $ctrl.model">
				<button class="clear" ui-sref="settings">Cancel</button>
				<button
					class="btn--dark"
					ng-disabled="$ctrl.form.$invalid"
					ng-click="$ctrl.update()">Update Settings</button>
			</div>
		</form>
 	</section>
 </section>
 </div>
`
};
