import { transformRequest } from "../utils/transformRequest"

export class ServicesService {
      constructor($resource, AppConfig) {
            "ngInject";

            const endPoint = `${AppConfig.apiEndpoint}/api/services`;

            this.api = $resource(`${endPoint}/:id`, { id: "@id" }, {
                  update: {
                        transformRequest: transformRequest,
                        headers: {
                              "Content-Type": undefined
                        },
                        method: "POST"
                  }
            });
      }
}
