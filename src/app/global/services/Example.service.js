import { transformRequest } from "../utils/transformRequest"

export class Example {
    constructor($resource, AppConfig) {
        "ngInject"
        const endPoint = `${AppConfig.apiEndpoint}categories`

        this.api = $resource(
            `${endPoint}/:id`,
            { id: "@id" },
            {
                update: {
                    method: "PUT"
                },
                save: {
                    url: endPoint,
                    method: "POST",
                    transformRequest: transformRequest,
                    headers: {
                        "Content-Type": undefined
                    }
                }
            }
        )
    }
}