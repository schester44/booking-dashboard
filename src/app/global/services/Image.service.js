import { transformRequest } from "../utils/transformRequest"

export class ImageService {
    constructor($resource, AppConfig) {
        "ngInject"
        const endPoint = `${AppConfig.apiEndpoint}/api/images`

        this.api = $resource(
            `${endPoint}/:id`,
            { id: "@id" },
            {
                update: {
                    method: "PUT"
                },
                save: {
                    url: endPoint,
                    method: "POST",
                    transformRequest: transformRequest,
                    headers: {
                        "Content-Type": undefined
                    }
                }
            }
        )
    }
}