export class DevicesService {
    constructor($resource, AppConfig) {
        "ngInject"
        const endPoint = `${AppConfig.apiEndpoint}/api/devices`

        this.api = $resource(endPoint)
    }
}