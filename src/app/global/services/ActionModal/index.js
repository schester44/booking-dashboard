import language from "./language"

export class ActionModal {
    constructor(ngDialog) {
        "ngInject"
        this.dialog = ngDialog

        this.language = language
    }

    open(action, opts) {

        if (typeof action !== "function") {
            throw new Error(`ActionModal:: First parameter must be a callback`);
            return;
        }

        const dialogOptions = {
            plain: true,
            appendClassName: "IWModal",
            width: 600,
            controllerAs: "$ctrl",
            template: require("./action.template.html"),
            controller: function() {

                Object.assign(this, opts)
                this.action = action
            }
        }
        
        if (opts.heading) {
            dialogOptions.appendClassName += " with-heading"
        }

        dialogOptions.showClose = opts.showClose ? opts.showClose : opts.heading ? true : false;

        return this.dialog.open(dialogOptions)
    }

    close(callback) {
        this.dialog.close(callback)
    }
}
