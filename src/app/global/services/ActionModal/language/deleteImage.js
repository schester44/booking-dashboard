export const deleteImage = {
	heading: "Delete Image",
	subHeading: "Are you sure you want to delete this image?",
	message: "Only delete this image if you're positive it is not in use by any kiosks.",
	button: "Yes, Delete"
}
