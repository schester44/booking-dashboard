export const uploadBanners = {
	heading: "Upload Banner",
	subHeading: "Uploaded banners will appear in the top bar of every page within the kiosk.",
	message: " If more than one image is added, images will rotate through in a loop. \n \n Recommended image size: 1010x220",
	button: "Select Files"
}
