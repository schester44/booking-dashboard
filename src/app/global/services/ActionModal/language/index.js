import { importSpreadsheet } from "./importSpreadsheet"
import { uploadBanners } from "./uploadBanners"
import { deleteImage } from "./deleteImage"

const language = {
	importSpreadsheet,
	uploadBanners,
	deleteImage
}

export default language