export const importSpreadsheet = {
	heading: "Import Spreadsheet",
	subHeading: "WARNING! Importing a spreadsheet will overwrite any previously imported data.",
	// message: "Maybe we should put a description here and yadda yadda yadda.. Tell the user what this is for.",
	button: "Select File"
}
