import { loadAppModule, ngmodule } from "../bootstrap/ngmodule"

/**
 * Services
 */
import { AppConfig } from "./services/AppConfig.service"
import { ActionModal } from "./services/ActionModal"
import { ServicesService } from "./services/Services.service"
import { DevicesService } from "./services/Devices.service"
import { ImageService } from "./services/Image.service"

/**
 * Filters
 */
import { num } from "./filters/num"
import { ucfirst } from "./filters/ucfirst"

/**
 * Export Module
 */
const globalAppModule = {
	components: {},
	directives: {},
	services: { AppConfig, ActionModal, ServicesService, DevicesService, ImageService },
	filters: { num, ucfirst },
	runBlocks: []
}

loadAppModule(ngmodule, globalAppModule)
