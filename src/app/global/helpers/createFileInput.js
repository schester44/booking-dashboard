/**
 * Accepts all attribute types (required, accepts, etc)
 * Accepts an object of eventhandlers ( { events: { change: this.onChange } } )
 * Accepts an object of additional data, accessible by input.data ( { data: { foo: 'bar' } })
 * 
 * @return DOM element - (createFile().click() will open the file input)
 */
export const createFileInput = options => {
	let input = document.createElement("input")
	input.setAttribute("type", "file")
	
	if (options.data) {
		input.data = options.data
	}
	
	Object.keys(options).forEach(key => {
		if (key !== "events" && key !== "data") {
			input.setAttribute(key, options[key])
		} else {
			Object.keys(options.events).forEach(eventKey => {
				input.addEventListener(eventKey, options.events[eventKey])
			})
		}
	})

	return input
}
