// similar to PHP's ucfirst function. Will uppercase the first letter of a word
export const ucfirst = () => input => input.split(" ").map(ch => ch.charAt(0).toUpperCase() + ch.substring(1)).join(" ");
