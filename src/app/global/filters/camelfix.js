// correct camelCase to human readable text
export const camelfix = () => input => input.replace(/\W+/g, " ").replace(/([a-z\d])([A-Z])/g, "$1 $2");
