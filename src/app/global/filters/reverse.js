// filter to reverse the order of an array
export const reverse = () => items => items.slice().reverse();
