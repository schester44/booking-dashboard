/**
 * Pass an element ID to this function and it will delete the node from the DOM
 */
export const deleteNode = id => {
	const tempNode = document.getElementById(id)

	if (tempNode) {
		tempNode.parentNode.removeChild(tempNode)
	}
}