/**
 * Primarily used in $resource services for converting the request data from application/json to a FormData object
 * @param  {object} data - your form data
 */

export const transformRequest = (data) => {
    const objectToFormData = (object, form, namespace) => {
      let fd = form || new FormData();
      let key;

      for (let property in object) {
        if (object.hasOwnProperty(property) && property.substr(0, 1) !== "$") {
          if (namespace) {
            key = namespace + "[" + property + "]";
          } else {
            key = property;
          }

          if (typeof object[property] === "object" && !(object[property] instanceof File)) {
            objectToFormData(object[property], fd, key);
          } else {
            fd.append(key, object[property]);
          }
        }
      }
      return fd;
    };

    return objectToFormData(data);
  };
  
