/**
 * Creates an file input element that can be used to trigger file dialog boxes and what not.
 * options can be attributes your typical file input accepts
 * for events, use options.events.<event-type> -- where event-type is your type of event (change)
 *
 * Example:
 *
 * this.input = createFileInput({ accept: "image/*", events: { change: this.onChange.bind(this) }})
 * this.input.click()
 * 
 */

export const createFileInput = options => {
	let input = document.createElement("input")
	input.setAttribute("type", "file")

	Object.keys(options).forEach(key => {
		if (key !== "events") {
			input.setAttribute(key, options[key])
		} else {
			Object.keys(options.events).forEach(eventKey => {
				input.addEventListener(eventKey, options.events[eventKey])
			})
		}
	})

	return input
}
