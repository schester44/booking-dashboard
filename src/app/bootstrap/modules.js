import * as angular from "angular"
import uiRouter from "@uirouter/angularjs"

// Optional --- 
import ngDialog from "ng-dialog"
import ngAnimate from "angular-animate"
import ngResource from "angular-resource"
import "@iamadamjowett/angular-click-outside"
import "angular-toastr"
import "ui-select"

import "../settings/components/theme/components/colorpicker"

export default  [
      uiRouter,
      ngAnimate,
      ngResource,
      ngDialog,
      "toastr",
      "ui.select",
      "angular-click-outside",
      "color.picker"
]