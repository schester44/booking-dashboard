//polyfills
import "babel-polyfill";

// global/app
import "../global";
import "../main";

// Scenes
import "../rooms";
import "../settings";
