"use strict"

// Modules
const webpack = require("webpack")
const autoprefixer = require("autoprefixer")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const ExtractTextPlugin = require("extract-text-webpack-plugin")
const CopyWebpackPlugin = require("copy-webpack-plugin")
const HardSourceWebpackPlugin = require("hard-source-webpack-plugin")
const HappyPack = require("happypack")

const path = require("path")
const ENV = process.env.npm_lifecycle_event

const isProd = ENV === "build"

const pkg = require("./package.json")

module.exports = (function makeWebpackConfig() {
  const config = {
    entry: {
      app: "./src/app/bootstrap/index.js"
    },
    output: {
      path: path.join(__dirname, "dist"),
      filename: isProd ? "[name].[hash].js" : "[name].bundle.js",
      chunkFilename: isProd ? "[name].[hash].js" : "[name].bundle.js"
    },
    devtool: isProd ? "source-map" : "eval-source-map",
    module: {
      rules: [
        {
          test: /\.js$/,
          loaders: ["happypack/loader"],
          exclude: /node_modules/
        },
        {
          test: /\.(scss|css)$/,
          loader: ExtractTextPlugin.extract({
            fallbackLoader: "style-loader",
            loader: [
              { loader: "css-loader" },
              { loader: "postcss-loader" },
              {
                loader: "sass-loader",
                query: {
                  data: "@import 'bootstrap';",
                  includePaths: [path.resolve(__dirname, "src/style")],
                  outputStyle: "compressed",
                  sourceMap: true
                }
              }
            ]
          })
        },
        {
          test: /\.(png|jpg|jpeg|gif)$/,
          loader: "file-loader"
        },
        {
          test: /\.(svg|woff|woff2|ttf|eot)/,
          loader: "file-loader?publicPath=../&name=./fonts/[hash].[ext]"
        },
        {
          test: /\.html$/,
          loader: "raw-loader"
        }
      ]
    },
    plugins: [
      new HardSourceWebpackPlugin({
        environmentHash: {
          files: [".npmrc"]
        }
      }),
      new HappyPack({
        loaders: ["babel-loader"]
      }),
      new webpack.LoaderOptionsPlugin({
        test: /\.scss$/i,
        options: {
          postcss: {
            plugins: [autoprefixer]
          }
        }
      }),
      new webpack.DefinePlugin({
        API_ENDPOINT: JSON.stringify(
          isProd ? process.env.npm_package_config_productionApiUrl : process.env.npm_package_config_devApiUrl
        ),
        APP_VERSION: pkg.version
      }),
      new HtmlWebpackPlugin({
        template: "./src/public/index.html",
        inject: "body"
      }),
      new ExtractTextPlugin({ filename: "css/[name].css", disable: !isProd, allChunks: true })
    ],
    devServer: {
      host: process.env.npm_config_booking_app_devHostIP,
      disableHostCheck: true,
      contentBase: "./src/public",
      stats: "minimal"
    }
  }

  if (isProd) {
    config.plugins.push(
      new webpack.NoEmitOnErrorsPlugin(),
      new webpack.optimize.UglifyJsPlugin({
        mangle: false,
        compress: {
          drop_console: true
        }
      }),
      new CopyWebpackPlugin([
        {
          from: path.join(__dirname, "src/public")
        }
      ])
    )
  }

  return config
})()
